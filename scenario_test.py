import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (AutomatTarget, AutomatType,
                              ScenarioContainer, StaticTarget, default_helis_list, load_network)


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.SA_3, g.Position(24.9792, 70.0577, 7), 0, 2),
        # StaticTarget(mpt.MPTarget.SHILKA, g.Position(24.9799, 70.0585, 7), 0, 2),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9946, 70.0493, 12), 0, 1),
    ]
    automat_2 = AutomatTarget('br-enna.fgfp', AutomatType.su_27, True, True,
                              True, True, 150,
                              4000, 8000, 0, False, 50,
                              True, 1, 2, 3, tacview=False)

    scenario = ScenarioContainer('Testing', 'Testing', '',
                                 (24., 69.5), (26., 70.5),
                                 'ENNA', polling_freq=10,
                                 lon_dem_grid_size=0.001, lat_dem_grid_size=0.0005,
                                 mp_visibility_range=300)
    scenario.add_static_targets(static_targets)
    # scenario.add_automats([automat_2])

    # -f /home/vanosten/custom_scenery/params.py -d /.../hunter-scenarios -b *24_69.5_26_70.5 -o test_helis
    # heli_network = load_network('test_helis.pkl', path)
    # scenario.add_helicopters(0, 1, heli_network, default_helis_list)

    return scenario
