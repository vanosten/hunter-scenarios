import os.path as osp

import hunter.geometry as g
import hunter.mp_carrier as mpc
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, StaticTarget, CarrierDefinition, TankerDefinition, AwacsDefinition,
                              AutomatTarget, AutomatType,
                              default_helis_list, default_drones_list, load_network,
                              ShipsDefinition, FILE_SHIP_NETWORKS, ShipNetworkParameters)


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        # Airport ENAT Alta
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.3584, 69.9709, g.feet_to_metres(187.)), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(23.2561, 69.8920, 22), 0, 1),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(23.3429, 69.9805, 0), 120),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(23.3466, 69.9798, 0), 120),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(23.3555, 69.9775, 3), 30),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(23.3830, 69.9722, 1), 217),
        StaticTarget(mpt.MPTarget.BRIDGE, g.Position(23.3750, 69.9685, 0), 152),
        StaticTarget(mpt.MPTarget.POWER_PLANT, g.Position(23.5154, 70.0239, 7), 0),
        # Airport ENHF Hammerfest
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(23.6673, 70.6805, 81), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(23.6752, 70.6820, 80), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(23.6513, 70.6789, 76), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(23.6793, 70.6803, 77), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.6665, 70.6796, 79), 0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(23.6776, 70.6827, 80), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.6745, 70.6855, 159), 0, 1),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(23.7201, 70.6806, 191), 0, 2),
        # Airport ENHV Honningsvåg
        # StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(25.9764, 71.0091, 3), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9703, 71.0093, 0), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9963, 71.0102, 3), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(25.9786, 71.0091, 5), 0),
        # StaticTarget(mpt.MPTarget.BUK_M2, g.Position(25.9803, 70.9878, 278), 0, 3),
        # StaticTarget(mpt.MPTarget.BUK_M2, g.Position(25.8925, 70.9844, 377), 0, 3),
        # airport ENNA Lakselv
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9792, 70.0577, 7), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9699, 70.064, 9), 85),  # along taxiway from south to north
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9688, 70.0686, 8), 85),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(24.9678, 70.0715, 5), 0),
        StaticTarget(mpt.MPTarget.HARD_SHELTER, g.Position(24.9676, 70.0737, 3.5), 270),  # north end
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(24.9730, 70.0555, 13), 45),  # south end
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(24.9847, 70.0628, 2), 110),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(24.9828, 70.0531, 8), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.9689, 70.0801, 1), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(24.9946, 70.0493, 12), 0, 1),
        # StaticTarget(wa.MPTarget.GASOMETER, g.Position(24.9818, 70.0669, 3), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(24.9833, 70.0648, 3), 0),
        # StaticTarget(mpt.MPTarget.OILRIG, g.Position(25.3508, 70.3837, 0), 0),
        # airport ENHK Hasvik
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(22.1347, 70.4861, 5), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(22.1421, 70.4848, 10), 0),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(22.2054, 70.4917, 46), 0, 2),
        StaticTarget(mpt.MPTarget.DVINA, g.Position(22.1351, 70.4966, 1), 0, 2),
        # StaticTarget(MPTarget.TRUCK, g.Position(22.1384, 70.4865, 10), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(22.1448, 70.4845, 12), 0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(22.1558, 70.4837, 0), 0),
        # North-East to ENAT along the street
        # StaticTarget(MPTarget.TRUCK, g.Position(23.8913, 70.1761, 383), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.9093, 70.1769, 352), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(23.9321, 70.1794, 345), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(23.9693, 70.1849, 326), 0, 3),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.5114, 70.4236, 85), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(24.5110, 70.4268, 77), 0, 3),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(24.5077, 70.4177, 107), 0),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.9109, 70.2044, 18), 0),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(24.9091, 70.2133, 41), 0, 1),

        # StaticTarget(MPTarget.TRUCK, g.Position(24.2908, 69.9208, 475), 0),
        StaticTarget(mpt.MPTarget.DVINA, g.Position(24.3015, 69.9244, 469), 0, 2),
        # StaticTarget(MPTarget.TRUCK, g.Position(24.2836, 69.9192, 468), 0),
        # StaticTarget(MPTarget.TRUCK, g.Position(24.2759, 69.9173, 468), 0),
    ]
    automat_1 = AutomatTarget('br-enhf.fgfp', AutomatType.mig_29, True, True,
                              True, True, 150,
                              4000, 8000, 0, False, 50,
                              True, 1, 1, 3, tacview=False)
    automat_2 = AutomatTarget('br-enna.fgfp', AutomatType.su_27, True, True,
                              True, True, 150,
                              4000, 8000, 0, False, 50,
                              True, 1, 2, 3, tacview=False)

    sail_area_carrier = [(18., 70.5), (20., 70.5), (20., 71.2), (18., 71.2)]
    vinson_carrier = CarrierDefinition(mpc.CarrierType.vinson, sail_area_carrier, (19., 70.85))
    tanker = TankerDefinition(19., 70., 19., 70.75, g.feet_to_metres(20000))
    awacs = AwacsDefinition(19., 70.2, 19., 70.5, g.feet_to_metres(25000))

    scenario = ScenarioContainer('north_norway', 'North Norway',  '',
                                 (18., 69.5), (27., 71.25),
                                 'ENNA', polling_freq=10, mp_visibility_range=300,
                                 dem_south_west=(21., 69.5), dem_north_east=(27., 71.25),
                                 lon_dem_grid_size=0.001, lat_dem_grid_size=0.0005)
    scenario.add_static_targets(static_targets)
    scenario.add_automats([automat_1, automat_2])
    scenario.add_carrier(vinson_carrier)
    scenario.add_tanker(tanker)
    scenario.add_awacs(awacs)

    # network created with heli_network.py: -f params.py -b *23_69.875_25.3_70.7 -o north_norway_helis
    # this network covers from ENAT to ENNA and up to Kvaløya
    heli_network = load_network('north_norway_helis.pkl', path)
    scenario.add_helicopters(3, 10, heli_network, default_helis_list)
    scenario.add_drones(3, 5, heli_network, default_drones_list)

    ship_network_parameters = ShipNetworkParameters(sail_area_offshore=[(21.7, 70.2), (24.5, 70.2), (24.5, 71.),
                                                                        (23., 71,), (21.7, 70.7)],
                                                    sail_area_coastal=[(24.5, 70.75), (23., 70.75), (22., 70.25),
                                                                       (23., 70.), (24.5, 70.125)],
                                                    max_level=6, min_level=4)
    ships_definition = ShipsDefinition(3, 6, 3, 10,
                                       ship_network_parameters,
                                       osp.join(path, scenario.ident.lower() + FILE_SHIP_NETWORKS))
    scenario.add_ships(ships_definition)
    return scenario
