import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import ScenarioContainer, StaticTarget


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        #Aquileia
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(13.373960, 45.789178, -0.83), 180.0, 0),
        #Plasencis
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(13.106389, 46.067638, 106.92), 60.0, 0),
        #Cordovado
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.911829, 45.826575, 9.23), 200.0, 0),
        #Caorle
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.822714, 45.626335, -0.49), 120.0, 0),
        #Sacile
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.548936, 45.945574, 21.80), 20.0, 0),
        #Cansiglio
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.399729, 46.069364, 1030.75), 340.0, 0),
        #Ceggia
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.670866, 45.674255, -1.9), 330.0, 0),
        #La Fossetta
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.456862, 45.580175, -2.55), 90.0, 0),
        #Peseggia
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(12.175656, 45.570817, 9.28), 45.0, 0),
        #Seren del Grappa
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(11.779777, 45.899726, 1437.75), 25.0, 0),
        #Base Tuono
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(11.229885, 45.870786, 1539.54), 160.0, 0),
        #Costozza
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(11.592551, 45.473717, 281.97), 140.0, 0),
        #Roncà
        StaticTarget(mpt.MPTarget.PATRIOT, g.Position(11.268700, 45.492584, 286.96), 100.0, 0),
    ]
    scenario = ScenarioContainer('italy', 'Italian SAM sites', 'Cold War Italian SAMs in NE Italy',
                                 (11.012811, 45.078259), (13.658063, 46.209146), 'LIPS',
                                 mp_visibility_range=250)
    scenario.add_static_targets(static_targets)
    return scenario