import os.path as osp

import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, StaticTarget, load_network, default_helis_list,
                              ShipsDefinition, ShipNetworkParameters, FILE_SHIP_NETWORKS)


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        # Port Howard SFPH
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-59.50651585, -51.62629475, 1), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-59.52009521, -51.63214343, 1), 0),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(-59.51432636, -51.61269427, 17), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.52070372, -51.61498746, 7), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.52368429, -51.61193519, 18), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.50628052, -51.61192936, 1), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-59.50338468, -51.62123505, 1), 0, 3),

        # Chartres SFCS
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.06014292, -51.71829692, 10), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.06904195, -51.71512290, 6), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.05780592, -51.72231465, 7), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.05508825, -51.72659592, 2), 0, 1),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.06391101, -51.73510859, 81), 0),

        # Dunnose Head SFDH
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.41455806, -51.75474579, 9), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.41779578, -51.75019133, 19), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.40498462, -51.75773028, 1), 0, 2),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.41800430, -51.76342062, 5), 0),

        # Shallow Harbour SFSW
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.52336272, -51.76004950, 16), 0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.52216032, -51.75124282, 40), 0),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(-60.51654578, -51.74090384, 111), 0, 2),

        # Spring Point SFSP
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.45442221, -51.83032886, 19), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.44393529, -51.83290163, 17), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.46448109, -51.83920252, 1), 0),
        StaticTarget(mpt.MPTarget.SA_3, g.Position(-60.48275161, -51.83544599, 10), 0, 1),

        # Port Stephens SFPS
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.83463924, -52.07935587, 18), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.85245786, -52.09274383, 17), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.84476475, -52.10766650, 1), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.84253921, -52.08965744, 22), 0, 2),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.84506353, -52.12238839, 1), 0),

        # West Point SFWP
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-60.66546029, -51.35547304, 3), 0, 1),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(-60.64381050, -51.34636112, 12), 0, 3),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(-60.69391652, -51.34264992, 12), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.68366466, -51.34916960, 10), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.63344255, -51.37084483, 16), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.69487360, -51.37863192, 2), 0),

        # Crodendle House SFCH
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-60.26388741, -51.52671908, 38), 0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(-60.25399085, -51.53174845, 30), 0, 3),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.25744850, -51.54523981, 46), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.25824423, -51.51302880, 56), 0),

        # Hill Cove SFHC
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.14780072, -51.50589489, 17), 0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.13912595, -51.50633961, 18), 0),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(-60.15864713, -51.48810510, 1), 0, 3),

        # North Arm SFNA
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-59.37846855, -52.12489388, 14), 0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.36832970, -52.12551483, 8), 0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(-59.37492797, -52.14119141, 7), 1),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-59.35872516, -52.13147986, 7), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(-59.38594129, -52.13293278, 12), 0),

        # Fox Bay SFFB
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(-60.06258578, -51.93542851, 29), 0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(-60.05899565, -51.95788521, 9), 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(-60.06694972, -51.92370182, 26), 0),
        StaticTarget(mpt.MPTarget.SA_3, g.Position(-60.07676724, -51.93897830, 6), 1),
    ]
    scenario = ScenarioContainer('falklands', 'Falklands',
                                 'West Falkland is occupied by OPFOR - and the southern part of '
                                 ' East Falkland.',
                                 (-61.5, -52.5), (-57.5, -51.),
                                 'EGYP', polling_freq=30, mp_visibility_range=250,
                                 dem_south_west=(-61.4, -52.25), dem_north_east=(-57.7, -51.2),
                                 lon_dem_grid_size=0.001, lat_dem_grid_size=0.0005)
    scenario.add_static_targets(static_targets)

    # network created with heli_network.py: -f TEST/params.py -b *-61_-52.25_-59.3_-51.2 -o falklands_helis.pkl -n -m
    # this network covers all of West Falkland plus some of the South-Western part of East Falkland
    # OSM data for Falklands extracted from south-america-latest
    # the DEM is larger than the heli network (but smaller than the scenario area) due to ships with long range missiles
    heli_network = load_network('falklands_helis.pkl', path)

    scenario.add_helicopters(0, 15, heli_network, default_helis_list)
    ship_network_parameters = ShipNetworkParameters(sail_area_offshore=[(-59.4, -51.), (-61.4, -51.),
                                                                        (-61.4, -52.3), (-59.9, -52.4)],
                                                    sail_area_coastal=[(-60., -52.25), (-59.75, -52.25), (-59., -51.6),
                                                                       (-60., -51.6)],
                                                    max_level=7, min_level=4)
    ships_definition = ShipsDefinition(0, 0, 0, 0,
                                       ship_network_parameters,
                                       osp.join(path, scenario.ident.lower() + FILE_SHIP_NETWORKS))
    scenario.add_ships(ships_definition)
    return scenario
