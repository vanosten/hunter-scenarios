from typing import List, Tuple

import networkx as nx

import hunter.mp_carrier as mpc
import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, load_network, default_helis_list,
                              CarrierDefinition, TankerDefinition)


def _construct_croatia_grgurov_kanal_speedboat_track() -> nx.Graph:
    # island of Prvić
    wp_1 = g.WayPoint(1, 14.766, 44.9358, alt_m=0)
    wp_2 = g.WayPoint(2, 14.7934, 44.9315, alt_m=0)
    wp_3 = g.WayPoint(3, 14.8354, 44.9045, alt_m=0)
    wp_4 = g.WayPoint(4, 14.8412, 44.8814, alt_m=0)
    wp_5 = g.WayPoint(5, 14.7643, 44.9109, alt_m=0)
    # island of Sveti Grgur
    wp_11 = g.WayPoint(11, 14.7457, 44.8878, alt_m=0)
    wp_12 = g.WayPoint(12, 14.7895, 44.8618, alt_m=0)
    wp_13 = g.WayPoint(13, 14.7718, 44.8519, alt_m=0)
    wp_14 = g.WayPoint(14, 14.7391, 44.8609, alt_m=0)
    wp_15 = g.WayPoint(15, 14.7317, 44.8769, alt_m=0)
    wp_16 = g.WayPoint(16, 14.7424, 44.8773, alt_m=0)
    # island of Goli otok
    wp_21 = g.WayPoint(21, 14.7963, 44.8536, alt_m=0)
    wp_22 = g.WayPoint(22, 14.8426, 44.8495, alt_m=0)
    wp_23 = g.WayPoint(23, 14.8400, 44.8356, alt_m=0)
    wp_24 = g.WayPoint(24, 14.8293, 44.8305, alt_m=0)
    wp_25 = g.WayPoint(25, 14.8300, 44.8204, alt_m=0)
    wp_26 = g.WayPoint(26, 14.7993, 44.8411, alt_m=0)
    # island of Rab
    wp_41 = g.WayPoint(41, 14.7678, 44.8472, alt_m=0)
    wp_42 = g.WayPoint(42, 14.7574, 44.8218, alt_m=0)
    wp_43 = g.WayPoint(43, 14.7497, 44.8150, alt_m=0)
    wp_44 = g.WayPoint(44, 14.8295, 44.7590, alt_m=0)
    wp_45 = g.WayPoint(45, 14.8784, 44.7215, alt_m=0)
    wp_46 = g.WayPoint(46, 14.8514, 44.6901, alt_m=0)
    wp_47 = g.WayPoint(47, 14.7951, 44.7336, alt_m=0)
    wp_48 = g.WayPoint(48, 14.7647, 44.7467, alt_m=0)
    wp_49 = g.WayPoint(49, 14.7594, 44.7436, alt_m=0)
    wp_50 = g.WayPoint(50, 14.6889, 44.7560, alt_m=0)
    wp_51 = g.WayPoint(51, 14.6523, 44.7918, alt_m=0)
    wp_52 = g.WayPoint(52, 14.6935, 44.8232, alt_m=0)
    wp_53 = g.WayPoint(53, 14.6716, 44.8467, alt_m=0)
    wp_54 = g.WayPoint(54, 14.7373, 44.8574, alt_m=0)

    # Coast
    wp_61 = g.WayPoint(61, 14.8720, 44.8162, alt_m=0)  # Kladd
    wp_62 = g.WayPoint(62, 14.8761, 44.7925, alt_m=0)  # Starigrad
    wp_63 = g.WayPoint(63, 14.8792, 44.7129, alt_m=0)  # Stinica

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5,
                          wp_11, wp_12, wp_13, wp_14, wp_15, wp_16,
                          wp_21, wp_22, wp_23, wp_24, wp_25, wp_26,
                          wp_41, wp_42, wp_43, wp_44, wp_45, wp_46, wp_47, wp_48, wp_49,
                          wp_50, wp_51, wp_52, wp_53, wp_54,
                          wp_61, wp_62, wp_63])

    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_3, wp_4)
    graph.add_edge(wp_4, wp_5)
    graph.add_edge(wp_5, wp_1)

    graph.add_edge(wp_11, wp_12)
    graph.add_edge(wp_12, wp_13)
    graph.add_edge(wp_13, wp_14)
    graph.add_edge(wp_14, wp_15)
    graph.add_edge(wp_15, wp_16)
    graph.add_edge(wp_16, wp_11)

    graph.add_edge(wp_21, wp_22)
    graph.add_edge(wp_22, wp_23)
    graph.add_edge(wp_23, wp_24)
    graph.add_edge(wp_24, wp_25)
    graph.add_edge(wp_25, wp_26)
    graph.add_edge(wp_26, wp_21)

    graph.add_edge(wp_5, wp_11)
    graph.add_edge(wp_4, wp_21)
    graph.add_edge(wp_12, wp_21)
    graph.add_edge(wp_13, wp_21)
    graph.add_edge(wp_13, wp_26)

    # Rab
    graph.add_edge(wp_41, wp_42)
    graph.add_edge(wp_42, wp_43)
    graph.add_edge(wp_43, wp_44)
    graph.add_edge(wp_44, wp_45)
    # not 45-46
    graph.add_edge(wp_45, wp_63)
    graph.add_edge(wp_63, wp_46)
    graph.add_edge(wp_46, wp_47)
    graph.add_edge(wp_47, wp_48)
    graph.add_edge(wp_48, wp_49)
    graph.add_edge(wp_49, wp_50)
    graph.add_edge(wp_50, wp_51)
    graph.add_edge(wp_51, wp_52)
    graph.add_edge(wp_52, wp_53)
    graph.add_edge(wp_53, wp_54)
    graph.add_edge(wp_54, wp_41)

    # coast and connections
    graph.add_edge(wp_61, wp_62)
    graph.add_edge(wp_62, wp_63)
    graph.add_edge(wp_45, wp_62)
    graph.add_edge(wp_23, wp_61)
    graph.add_edge(wp_25, wp_61)
    graph.add_edge(wp_14, wp_54)
    graph.add_edge(wp_13, wp_41)
    graph.add_edge(wp_26, wp_41)

    return graph


def _construct_sail_area_carrier_adria() -> List[Tuple[float, float]]:
    return [(13.66, 44.83),  # Pula
            (12.5, 44.5),  # Ravenna
            (13.6, 43.67),  # Ancona
            (14.35, 42.55),  # Pescara
            (16.0, 43.28)  # Split
            ]


def build_scenario(path: str) -> ScenarioContainer:
    speedboat_graph = _construct_croatia_grgurov_kanal_speedboat_track()
    origin_1 = g.Position(14.766, 44.9358)  # WP_1
    speedboat_1 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.SPEEDBOAT, speedboat_graph, origin_1,
                                                         mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)
    origin_2 = g.Position(14.8300, 44.8204)  # WP_25
    speedboat_2 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.SPEEDBOAT, speedboat_graph, origin_2,
                                                         mpt.MPTargetTrips.STATIC_HP, 5, 10000, 0)

    trip_targets = [speedboat_1, speedboat_2]

    vinson_carrier = CarrierDefinition(mpc.CarrierType.vinson, _construct_sail_area_carrier_adria(), (14.25, 44.0))

    tanker = TankerDefinition(13.43, 44.1, 14.17, 44.1, g.feet_to_metres(20000))

    # network created with heli_network.py: -f TEST/params.py -b *14.25_43.625_16_45 -o kornati_helis.pkl -n -m
    # => Can skip OSM data for Bosnia -> no POI in that area in the top right corner of the scenario
    heli_network = load_network('kornati_helis.pkl', path)

    scenario = ScenarioContainer('kornati', 'Kornati',
                                 'Croatian coast between Krk and Sibenik with helis and speedboats',
                                 (12.5, 42.55), (16.0, 45.0),
                                 'LDRI', polling_freq=60, mp_visibility_range=250,
                                 lon_dem_grid_size=0.0005)
    scenario.add_targets_with_trips(trip_targets)
    scenario.add_carrier(vinson_carrier)
    scenario.add_tanker(tanker)
    scenario.add_helicopters(5, 8, heli_network, default_helis_list)
    return scenario
