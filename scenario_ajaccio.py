import os.path as osp
import random

import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, StaticTarget, load_network, default_helis_list,
                              ShipsDefinition, ShipNetworkParameters, FILE_SHIP_NETWORKS)


def build_scenario(path: str) -> ScenarioContainer:
    vehicles = [mpt.MPTarget.SHILKA, mpt.MPTarget.BRADLEY_TANK, mpt.MPTarget.M1_TANK, mpt.MPTarget.TRUCK,
                mpt.MPTarget.STRYKER_TANK]
    static_targets = [
        # hill North-East
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(8.7900, 41.9440, 217), random.randint(0, 355)),

        # hill South of Porticcio
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(8.79378, 41.8814, 83), random.randint(0, 355)),

        # land near parking West
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(8.8020, 41.9171, 2), random.randint(0, 355)),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(8.8031, 41.9173, 3), random.randint(0, 355)),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(8.8037, 41.9177, 2), random.randint(0, 355)),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(8.8033, 41.9186, 1), random.randint(0, 355)),

        # South end of runway 02/20
        StaticTarget(random.choice(vehicles), g.Position(8.7944, 41.9107, 0), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.7957, 41.9110, 0), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.7968, 41.9120, 0), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.7971, 41.9125, 0), random.randint(0, 355)),

        # north end of runway 02/20
        StaticTarget(random.choice(vehicles), g.Position(8.8077, 41.9354, 3), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8090, 41.9339, 2), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8137, 41.9354, 5), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8110, 41.9392, 5), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8116, 41.9369, 5), random.randint(0, 355)),

        # East end of airport
        StaticTarget(random.choice(vehicles), g.Position(8.8119, 41.9185, 10), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8116, 41.9169, 17), random.randint(0, 355)),
        StaticTarget(random.choice(vehicles), g.Position(8.8078, 41.9151, 4), random.randint(0, 355)),

        # parking West
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.8036, 41.9204, 0), 280),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.8034, 41.9201, 0), 280),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.8032, 41.9198, 0), 280),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.8030, 41.9196, 0), 280),

        # main apron
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7962, 41.9202, 1), 160),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7966, 41.9208, 1), 160),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7970, 41.9215, 1), 160),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7974, 41.9221, 1), 160),

        # North-East apron
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7914, 41.9254, 2), 55),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7914, 41.9257, 2), 145),
        StaticTarget(mpt.MPTarget.VIGGEN_STATIC, g.Position(8.7919, 41.9257, 2), 235),
    ]
    scenario = ScenarioContainer('ajaccio', 'Ajaccio Corsica',
                                 'The bay of Ajaccio and the LFKJ airport are occupied by OPFOR',
                                 (8, 41.25), (10, 42.5),
                                 'LFKS', polling_freq=30, mp_visibility_range=150,
                                 dem_south_west=(8.5, 41.375), dem_north_east=(9.25, 42.5),
                                 lon_dem_grid_size=0.001, lat_dem_grid_size=0.0005)
    scenario.add_static_targets(static_targets)

    # The network covers an area along the Western coast of Corsica from the airport of Propriano in the South
    # to the next bay to the North of Ajaccio
    # It was created with: -b *8.5_41.625_9_42.125 -o ajaccio_helis -m
    heli_network = load_network('ajaccio_helis.pkl', path)

    scenario.add_helicopters(3, 10, heli_network, default_helis_list)
    ship_network_parameters = ShipNetworkParameters(sail_area_offshore=[(8., 41.25), (9., 41.25),
                                                                        (9., 42.0), (8., 42.0)],
                                                    sail_area_coastal=[(8.5, 41.625), (9., 41.625),
                                                                       (9., 42.125), (8., 42.125)],
                                                    max_level=7, min_level=4)
    ships_definition = ShipsDefinition(2, 3, 3, 6,
                                       ship_network_parameters,
                                       osp.join(path, scenario.ident.lower() + FILE_SHIP_NETWORKS))
    scenario.add_ships(ships_definition)
    return scenario
