import os.path as osp

import hunter.mp_carrier as mpc
from hunter.scenarios import (ScenarioContainer, ShipsDefinition, ShipNetworkParameters, FILE_SHIP_NETWORKS,
                              CarrierDefinition, default_helis_list, default_drones_list, load_network)


def build_scenario(path: str) -> ScenarioContainer:
    scenario = ScenarioContainer('cyclades', 'Cyclades islands in Greece with ships and helicopters',
                                 'Ships and helicopters/drones around the islands between Athens and '
                                 'Crete in Greece.',
                                 (23., 36.), (26., 38.125),
                                 'LGEL', polling_freq=30, mp_visibility_range=300,
                                 dem_south_west=(23.3, 36.6), dem_north_east=(25.5, 37.7))
    ships_definition = ShipsDefinition(2, 6, 3, 6,
                                       ShipNetworkParameters(sail_area_offshore=[(23., 36.6), (26., 36.6),
                                                                                 (26., 37.5), (23., 38)],
                                                             sail_area_coastal=[(23., 37.), (24.2, 37.),
                                                                                (24.2, 38.), (23., 38.)],
                                                             max_level=7, min_level=4),
                                       osp.join(path, scenario.ident.lower() + FILE_SHIP_NETWORKS))
    scenario.add_ships(ships_definition)

    sail_area_carrier = [(26., 37.7), (26., 38.125), (25., 38.125), (25., 37.9)]
    vinson_carrier = CarrierDefinition(mpc.CarrierType.vinson, sail_area_carrier, (25.5, 37.9))
    scenario.add_carrier(vinson_carrier)

    # network created with heli_network.py: -f params.py -b *25_36.9_25.5_37.2 -o cyclades_helis.pkl
    # smaller boundary than DEM: islands of Antiparos, Paros and Naxos
    heli_network = load_network('cyclades_helis.pkl', path)
    scenario.add_helicopters(3, 5, heli_network, default_helis_list)
    scenario.add_drones(3, 5, heli_network, default_drones_list)

    return scenario
