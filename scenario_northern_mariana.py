import os.path as osp

import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import ScenarioContainer, StaticTarget, ShipsDefinition, ShipNetworkParameters, FILE_SHIP_NETWORKS


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.SA_6, g.Position(145.66579653, 14.95363634, 2.8763), 0, 2),
        StaticTarget(mpt.MPTarget.S_300, g.Position(145.65159095, 14.95604869, 182.8701), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(145.55179930, 14.84919640, 157.4966), 0, 2),
        StaticTarget(mpt.MPTarget.SA_3, g.Position(145.62759909, 15.04078500, 154.6720), 0, 2),
        StaticTarget(mpt.MPTarget.DVINA, g.Position(145.63128228, 15.01646323, 83.6044), 240.0, 1),
        StaticTarget(mpt.MPTarget.TOWER, g.Position(145.72406157, 15.12155346, 65.5575), 85.0),
        # StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72547640, 15.12485989, 63.1279), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.7267, 15.1258, 63.1279), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72545999, 15.12391426, 63.6506), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72694381, 15.12315665, 65.5359), 0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(145.72666452, 15.12467371, 63.6134), 0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.72881483, 15.12316435, 66.2967), 204.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.72949293, 15.12345911, 66.3027), 204.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(145.73020012, 15.12377071, 66.3147), 204.0),
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(145.72379214, 15.12331304, 62.7219), 292.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(145.72582705, 15.12042629, 67.4474), 148.0)
    ]
    scenario = ScenarioContainer('northern_mariana', 'Northern Mariana',
                                 '',
                                 (144.5, 13.2), (146, 15.375),
                                 'PGUA', polling_freq=60, mp_visibility_range=250)
    scenario.add_static_targets(static_targets)
    ships_definition = ShipsDefinition(2, 4, 0, 0,
                                       ShipNetworkParameters(sail_area_offshore=[(145.25, 14.5), (146, 14.5),
                                                                                 (146, 15.375), (145.25, 15.375)],
                                                             sail_area_coastal=[(145.25, 14.5), (146, 14.5),
                                                                                (146, 15.375), (145.25, 15.375)]),
                                       osp.join(path, scenario.ident.lower() + FILE_SHIP_NETWORKS))
    scenario.add_ships(ships_definition)
    return scenario
