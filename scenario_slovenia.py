import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import ScenarioContainer, StaticTarget


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(14.27396873, 45.73540557, 914.56), 281.5, 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(14.25138019, 45.73628135, 611.19), 263, 0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(14.25440574, 45.74697170, 736.68), 55),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(14.25402719, 45.74658945, 730.62), 55),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(14.24157177, 45.74707746, 650.97), 39),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(14.24064588, 45.74621311, 639.10), 39),
        StaticTarget(mpt.MPTarget.CLIFF_TARGET, g.Position(14.25721968, 45.74008784, 673.50), 35),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(14.26763042, 45.73581533, 870.17), 50.0, 1),
        StaticTarget(mpt.MPTarget.STRYKER_TANK, g.Position(14.23264736, 45.74338950, 581.08), 140, 0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(14.23288881, 45.74321430, 579.00), 140, 0),
        StaticTarget(mpt.MPTarget.STRYKER_TANK, g.Position(14.23312140, 45.74304204, 576.85), 140, 0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(14.23335668, 45.74287666,  575.66), 140, 0),
        StaticTarget(mpt.MPTarget.STRYKER_TANK, g.Position(14.23364837, 45.74267370, 573.96), 140, 0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23779725, 45.74019298, 607.32), 92),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23655757, 45.74026464, 595.41), 268),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23711567, 45.74058472, 598.82), 356),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23716413, 45.73984335, 601.96), 0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23537241, 45.73735888, 591.61), 0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.24458587, 45.73931009, 573.32), 275),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(14.23574937, 45.73772678, 594.17), 90),
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(14.23085619, 45.73722545, 582.3274), 123),
        # Postojna SAM site
        StaticTarget(mpt.MPTarget.SA_6, g.Position(14.205954, 45.788676, 635.67), 180.0, 1),
    ]
    scenario = ScenarioContainer('slovenia', 'Pocek', 'Vojaski Poligon Pocek',
                                 (13.97, 45.66), (14.27, 45.8),
                                 'LJDI', mp_visibility_range=250,
                                 lon_dem_grid_size=0.0005)
    scenario.add_static_targets(static_targets)
    return scenario
