import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, StaticTarget, load_network, default_helis_list)


def build_scenario(path: str) -> ScenarioContainer:
    scenario = ScenarioContainer('martinique', 'Some fantasy stuff around the French island Martinique',
                                 'Mostly things to shoot at with cannon and such.',
                                 (-61.25, 14.375), (-60.75, 15.0),
                                 'TFFF', polling_freq=60, mp_visibility_range=150)

    static_targets = [
        # Baie du Trésor
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(-60.8758, 14.7576, 0), 130),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(-60.8951, 14.7596, 0), 180),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(-60.8835, 14.7660, 0), 110)
    ]

    # -f /home/vanosten/custom_scenery/params.py -o martinique_roads.pkl
    # -d /home/vanosten/develop_hunter/hunter-scenarios -m 13 -w 25 -l 100 -b *-61.25_14.375_-60.75_15.0
    roads_trip_graph = load_network('martinique_roads.pkl', path)
    truck_1 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, roads_trip_graph, None,
                                                     mpt.MAX_HITPOINTS_VEHICLE, 10, 5000, 0)
    truck_2 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, roads_trip_graph, None,
                                                     mpt.MAX_HITPOINTS_VEHICLE, 10, 5000, 0)
    truck_3 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, roads_trip_graph, None,
                                                     mpt.MAX_HITPOINTS_VEHICLE, 10, 5000, 0)
    truck_4 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, roads_trip_graph, None,
                                                     mpt.MAX_HITPOINTS_VEHICLE, 10, 5000, 0)
    truck_5 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.TRUCK, roads_trip_graph, None,
                                                     mpt.MAX_HITPOINTS_VEHICLE, 10, 5000, 0)
    buk_1 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.BUK_M2, roads_trip_graph, None,
                                                   mpt.MAX_HITPOINTS_TANK, 60, 5000, 0)
    buk_2 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.BUK_M2, roads_trip_graph, None,
                                                   mpt.MAX_HITPOINTS_TANK, 60, 5000, 0)
    buk_3 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.BUK_M2, roads_trip_graph, None,
                                                   mpt.MAX_HITPOINTS_TANK, 60, 5000, 0)
    buk_4 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.BUK_M2, roads_trip_graph, None,
                                                   mpt.MAX_HITPOINTS_TANK, 60, 5000, 0)
    buk_5 = mpt.MPTargetTrips.create_random_target(mpt.MPTarget.BUK_M2, roads_trip_graph, None,
                                                   mpt.MAX_HITPOINTS_TANK, 60, 5000, 0)

    trip_targets = [truck_1, truck_2, truck_3, truck_4, truck_5,
                    buk_1, buk_2, buk_3, buk_4, buk_5]

    # -f /home/vanosten/custom_scenery/params.py -o martinique_heli.pkl
    # -d /home/vanosten/develop_hunter/hunter-scenarios -b *-61.25_14.375_-60.75_15.0
    heli_network = load_network('martinique_helis.pkl', path)

    scenario.add_static_targets(static_targets)
    scenario.add_targets_with_trips(trip_targets)
    scenario.add_helicopters(2, 6, heli_network, default_helis_list)

    return scenario
