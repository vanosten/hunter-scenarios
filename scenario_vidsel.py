import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import ScenarioContainer, StaticTarget


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.RADAR_STATION, g.Position(19.31797133, 66.39282943, 744.3719), 16.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(19.31513378, 66.39379173, 739.0978), 120.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31950926, 66.38849880, 693.8754), 144.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31904186, 66.38835333, 693.9146), 144.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.31863179, 66.38822920, 693.5738), 144.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.30359813, 66.40229434, 622.7536), 144.0),
        StaticTarget(mpt.MPTarget.HANGAR, g.Position(19.31127886, 66.39751473, 668.6769), 48.0),
        StaticTarget(mpt.MPTarget.HANGAR, g.Position(19.31045241, 66.39787278, 665.7117), 48.0),
        StaticTarget(mpt.MPTarget.S_300, g.Position(19.30464126, 66.40767511, 658.5365), -24.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.31605033, 66.39240587, 738.7836), -168.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.30184618, 66.40491959, 626.9441), -96.0),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(19.54349283, 66.26228163, 476.9974), 72.0),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(19.27394310, 66.35653929, 459.4473), 24.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40638195, 66.30230167, 641.8291), -168.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40369599, 66.30258359, 644.2588), -168.0),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(19.40321754, 66.29985320, 656.4868), -168.0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(19.20640023, 66.31270131, 589.9165), -96.0),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(19.20908678, 66.31575506, 596.0900), -24.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.20882646, 66.31381915, 592.3608), 0.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.21143275, 66.31377540, 593.0204), 0.0),
        StaticTarget(mpt.MPTarget.CONTAINERS, g.Position(19.21143915, 66.31202278, 590.4141), -52.0),
        StaticTarget(mpt.MPTarget.HARD_SHELTER, g.Position(19.21291479, 66.32204313, 602.9526), 0.0),
        StaticTarget(mpt.MPTarget.DOUBLE_SHELTER, g.Position(19.21634312, 66.32102837, 607.1071), 0.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(19.21644677, 66.31984714, 604.9741), -88.0),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(19.21643993, 66.31920160, 603.6729), -88.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.21058105, 66.32030831, 604.1210), 0.0),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(19.20065322, 66.33814333, 637.3965), 96.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.19668719, 66.33828890, 639.1387), -24.0),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(19.19737021, 66.33849553, 637.1038), -24.0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(19.19362982, 66.33854382, 633.5181), -24.0),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(19.19893407, 66.32989331, 617.0047), 120.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.48019603, 66.25903261, 601.1159), 0.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.46090564, 66.26839320, 658.8787), 0.0),
        StaticTarget(mpt.MPTarget.GASOMETER, g.Position(19.46517561, 66.26676383, 652.3761), 0.0),
        StaticTarget(mpt.MPTarget.FUEL_FARM, g.Position(19.46220665, 66.26716873, 659.1560), 24.0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(19.47733422, 66.26138108, 615.9012), -72.0),
        StaticTarget(mpt.MPTarget.LIGHT_HANGAR, g.Position(19.47423907, 66.26184409, 626.6799), -72.0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(19.45869373, 66.25975203, 632.3859), -72.0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(19.45983429, 66.26061953, 637.5146), -72.0),
        StaticTarget(mpt.MPTarget.FACTORY, g.Position(19.46658283, 66.25928029, 624.8444), 12.0),
        StaticTarget(mpt.MPTarget.POWER_PLANT, g.Position(19.45015968, 66.26757949, 658.3874), 48.0),
    ]
    scenario = ScenarioContainer('vidsel', 'Vidsel test range', 'Vidsel test range',
                                 (18.0, 65.7), (20.5, 67.0),
                                 'ESNJ', polling_freq=60, mp_visibility_range=250)
    scenario.add_static_targets(static_targets)
    return scenario
