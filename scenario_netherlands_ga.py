import networkx as nx

import hunter.geometry as g
from hunter.scenarios import (ScenarioContainer, default_ga_list)


def _construct_ga_network() -> nx.Graph:
    wp_1 = g.WayPoint(1, 5.52, 52.45)  # Lelystad EHNL
    wp_2 = g.WayPoint(2, 6.58, 53.12)  # Groningen EHGG
    wp_3 = g.WayPoint(3, 5.75, 53.21)  # Leeuwarden EHLW
    wp_4 = g.WayPoint(4, 4.78, 52.92)  # Den Helder EHKD
    wp_5 = g.WayPoint(5, 6.88, 52.27)  # Enschede/Twente EHTW
    wp_6 = g.WayPoint(6, 4.74, 52.33)  # Schiphol EHAM
    wp_7 = g.WayPoint(7, 4.44, 51.95)  # Rotterdam EHRD
    wp_8 = g.WayPoint(8, 4.93, 51.56)  # Gilze-Reijen EHGR
    wp_9 = g.WayPoint(9, 5.37, 51.45)  # Eindhoven EHEH
    wp_10 = g.WayPoint(10, 5.69, 51.65)  # Uden EHVK
    wp_11 = g.WayPoint(11, 4.33, 51.44)  # Woensdrecht EHWO

    graph = nx.Graph()
    graph.add_nodes_from([wp_1, wp_2, wp_3, wp_4, wp_5, wp_6,
                         wp_7, wp_8, wp_9, wp_10, wp_11])
    for wp in graph.nodes:
        wp.is_point_of_interest = True


    graph.add_edge(wp_1, wp_2)
    graph.add_edge(wp_1, wp_3)
    graph.add_edge(wp_1, wp_4)
    graph.add_edge(wp_1, wp_5)
    graph.add_edge(wp_1, wp_6)
    graph.add_edge(wp_1, wp_9)

    graph.add_edge(wp_2, wp_3)
    graph.add_edge(wp_2, wp_5)

    graph.add_edge(wp_3, wp_4)

    graph.add_edge(wp_4, wp_6)

    graph.add_edge(wp_5, wp_10)

    graph.add_edge(wp_6, wp_7)
    graph.add_edge(wp_6, wp_9)

    graph.add_edge(wp_7, wp_11)
    graph.add_edge(wp_7, wp_8)

    graph.add_edge(wp_8, wp_11)
    graph.add_edge(wp_8, wp_9)
    graph.add_edge(wp_8, wp_10)

    graph.add_edge(wp_9, wp_10)

    return graph


def build_scenario(path: str) -> ScenarioContainer:
    ga_graph = _construct_ga_network()

    scenario = ScenarioContainer('netherlands_ga', 'General aviation in the Netherlands (mostly Holland)',  '',
                                 (3, 51.5), (7, 53.5),
                                 'EHEL', polling_freq=60, mp_visibility_range=250)
    scenario.add_general_aviation(3, 6, ga_graph, default_ga_list)
    return scenario
