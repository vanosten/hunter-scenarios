import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import (ScenarioContainer, StaticTarget)


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.45134603, 45.12084095, 315.22), 190),
        StaticTarget(mpt.MPTarget.CHECKPOINT, g.Position(15.47651057, 45.15617316, 275.65), 223),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.46642110, 45.15227261, 258.38), 225),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(15.35403661, 45.17658397, 415.64), 350),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(15.35605100, 45.17734675, 407.75), 350),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(15.35764889, 45.17402033, 461.74), 350),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.44269778, 45.13236673, 284.48), 105),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.43930508, 45.13526179, 285.17), 105),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.43756785, 45.13230291, 295.75), 105),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.47054590, 45.14772888, 259.03), 121),
        StaticTarget(mpt.MPTarget.BRADLEY_TANK, g.Position(15.48843116, 45.13102892, 279.85), 330),
        StaticTarget(mpt.MPTarget.BRADLEY_TANK, g.Position(15.48885112, 45.13049413, 280.77), 330),
        StaticTarget(mpt.MPTarget.BRADLEY_TANK, g.Position(15.48919529, 45.13005391, 281.47), 330),
        StaticTarget(mpt.MPTarget.BRADLEY_TANK, g.Position(15.48958546, 45.12957522, 280.956), 330),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.49388525, 45.09844544, 279.30), 120),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.49090056, 45.09978517, 284.96), 120),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48753297, 45.10134146, 293.71), 120),
        StaticTarget(mpt.MPTarget.TRUCK, g.Position(15.48461009, 45.10268827, 289.39), 120),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.49277869, 45.11349851, 282.90), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48581480, 45.11163144, 286.13), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48928243, 45.11853318, 282.78), 70),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(15.48252808, 45.11650189, 293.97), 70),
        StaticTarget(mpt.MPTarget.MLRS_ROCKET_LAUNCHER, g.Position(15.50651187, 45.10753776, 275.20), 0),
        StaticTarget(mpt.MPTarget.MLRS_ROCKET_LAUNCHER, g.Position(15.50637840, 45.10665494, 277.80), 0),
        StaticTarget(mpt.MPTarget.MLRS_ROCKET_LAUNCHER, g.Position(15.50528944, 45.10668555, 280.69), 0),
        StaticTarget(mpt.MPTarget.MLRS_ROCKET_LAUNCHER, g.Position(15.50540209, 45.10758750, 278.10), 0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.52031985, 45.09005467, 274.88), 336),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(15.52174861, 45.08477434, 288.44), 336),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(15.56593041, 45.05902512, 427.084), 30),
        StaticTarget(mpt.MPTarget.CONTARGET, g.Position(15.56397648, 45.06171625, 444.30), 30),
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(15.51104405, 45.11098530, 263.94), 240),
        # SAMs + AAAs
        StaticTarget(mpt.MPTarget.S_300, g.Position(15.54266675, 45.03965636, 373.13), 35, 1),
        StaticTarget(mpt.MPTarget.DVINA, g.Position(15.42672690, 45.18603622, 250.04), 105, 1),
        StaticTarget(mpt.MPTarget.SA_6, g.Position(15.48677027, 45.08705502, 374.41), 24, 1),
        StaticTarget(mpt.MPTarget.SA_3, g.Position(15.44195858, 45.17045487, 256.01), 0, 1),
        StaticTarget(mpt.MPTarget.BUK_M2, g.Position(15.46779326, 45.13225982, 300.99), 265, 1),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.48616894, 45.15405882, 359.11), 96),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.47002046, 45.11503543, 367.80), 310),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.52217412, 45.06211653, 526.34), 160),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(15.52896415, 45.10106624, 318.51), 180),
        # lake / sump at South-West corner just a bit outside
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4052, 45.0218, 484), 80),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4058, 45.0181, 484), 260),
        StaticTarget(mpt.MPTarget.WATER_TARGET, g.Position(15.4136, 45.0162, 485), 180),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(15.4246, 45.0212, 607), 60),
        StaticTarget(mpt.MPTarget.HILL_TARGET, g.Position(15.4239, 45.0224, 617), 60)
    ]

    scenario = ScenarioContainer('croatia', 'Eugen Kvaternik (Slunj)', 'Croatian Military Range near Slunj',
                                 (14.25, 43.625), (16., 45.25),
                                 'LDRI', polling_freq=60, mp_visibility_range=250,
                                 # the DEM grid is just a copy of the Kornati one
                                 dem_south_west=(14.25, 43.625), dem_north_east=(16.0, 45.0),
                                 lon_dem_grid_size=0.0005)
    scenario.add_static_targets(static_targets)
    return scenario
