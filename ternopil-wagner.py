import hunter.geometry as g
import hunter.mp_targets as mpt
from hunter.scenarios import ScenarioContainer, StaticTarget


def build_scenario(path: str) -> ScenarioContainer:
    static_targets = [
    #Wagner facilities
        StaticTarget(mpt.MPTarget.BUNKER, g.Position(25.61672676, 49.53419013, 344.22), 218, 0),
        StaticTarget(mpt.MPTarget.DEPOT, g.Position(25.61613432, 49.52796965, 346.70), 160, 0),
        StaticTarget(mpt.MPTarget.FACTORY, g.Position(25.61042036, 49.53239038, 339.56), 337, 0),
        StaticTarget(mpt.MPTarget.COMPOUND, g.Position(25.61560301, 49.53296294, 342.18), 229, 0),
        StaticTarget(mpt.MPTarget.FUEL_FARM, g.Position(25.61578284, 49.53161416, 339.51), 240, 0),
        StaticTarget(mpt.MPTarget.WAREHOUSE, g.Position(25.62062267, 49.56580914, 330.32), 336, 0),
    #AAA
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(25.61638624, 49.53340025, 346.74), 312, 1),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(25.61194513, 49.53246319, 338.87), 336, 1),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(25.61645059, 49.52811277, 347.84), 48, 1),
        StaticTarget(mpt.MPTarget.SHILKA, g.Position(25.61872549, 49.53154779, 348.96), 96, 1),
    #Civilian structures
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.61693859, 49.53979825, 327.01), 332, 0),
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.60992663, 49.53975790, 316.47), 96, 0),
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.61372619, 49.53943949, 320.12), 176, 0),
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.61712814, 49.56182875, 322.83), 28, 0),
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.61499086, 49.56451803, 329.70), 296, 0),
        StaticTarget(mpt.MPTarget.HQ, g.Position(25.62303031, 49.55053997, 361.88), 336, 0),
        StaticTarget(mpt.MPTarget.BRIDGE, g.Position(25.60452642, 49.53144609, 328.70), 56.5, 0),
        StaticTarget(mpt.MPTarget.POWER_PLANT, g.Position(25.58786228, 49.56589006, 313.71), 104, 0),
    ]
    scenario = ScenarioContainer('ternopil_wagner', 'ternopil_wagner', 'Wagner facilities in Ukraine', (25.47726134, 49.62937395), (25.59796849, 49.50542221), 'UKLT', mp_visibility_range=300)
    scenario.add_static_targets(static_targets)
    return scenario